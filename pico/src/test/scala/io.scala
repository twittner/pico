package test.pico.io

import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.charset.Charset

import pico.Pico._

final class IoTest {

  private val charset = Charset.forName("UTF-8")
  private val server = asyncServerSocket()

  def handleClient(client: AsyncSocket): Unit =
          fork (onShutdown = println("good bye"), body = {
                  println("reading from client ...")
                  val buffer = ByteBuffer.allocateDirect(1024)
                  handleAll log {
                          val n = client.read(buffer)
                          buffer.flip()
                          val s = charset.decode(buffer).toString
                          println("read %d bytes: %s".format(n, s))
                          println("closing client connection ...")
                          client.close()
                  }
          })

  fork {
          server.bind(new InetSocketAddress(8000), 32)
          while (server.isOpen) {
                  println("waiting for connection ...")
                  handleClient(server.accept())
                  println("and accepting another one ...")
          }
  }

  def run() {
          Thread.sleep(30000)
          shutdown()
  }
}

object Test {
  def main(args: Array[String]) {
          new IoTest().run()
  }
}
