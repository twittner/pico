package test.pico

import pico._
import pico.schedulers.DefaultScheduler
import java.util.concurrent.atomic.AtomicInteger

final class SchedulerTest(scheduler: Scheduler, n: Int) {

  import scheduler._

  private val i = new AtomicInteger(0)

  fork {
          var j = i.getAndIncrement()
          while (j < n) {
                  println("A: " + j)
                  suspend()
                  j = i.getAndIncrement()
          }
  }

  fork {
          var j = i.getAndIncrement()
          while (j < n) {
                  println("B: " + j)
                  suspend()
                  j = i.getAndIncrement()
          }
  }

  fork {
          (0 until (n / 2)).cps.foreach { _ => {
                  println("---")
                  suspend()
          }}
  }

  def run() {
          while (i.get < n)
                  Thread.sleep(100)
          scheduler.shutdown()
  }
}

object TestConcurrent {

  def main(args: Array[String]) {
          // single-threaded:
          new SchedulerTest(new DefaultScheduler(1), 10000).run()
          // multi-threaded:
          new SchedulerTest(new DefaultScheduler(), 10000).run()
  }
}


