package test.pico.bench

import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.util.concurrent.{ThreadLocalRandom => Rand}

import pico._
import pico.Pico._

object Pong {

  private val server = asyncServerSocket()
  private val payload = "HTTP/1.1 200 OK\r\nContent-Length: 1024\r\n\r\n".getBytes

  private def pong(client: AsyncSocket): Unit = fork {
          val buffer = ByteBuffer.allocateDirect(1071)

          if (client.read(buffer) != -1) {
                  handleAll result {
                          buffer.clear()
                          buffer.put(payload)
                          for (_ <- 0 until 1024)
                                  buffer.put(Rand.current.nextInt(128).asInstanceOf[Byte])
                          buffer.flip()
                          client.write(buffer)
                  } fold (_.printStackTrace(), _ => println("okay"))
          }

          client.close()
  }

  fork {
          server.bind(new InetSocketAddress(8000), 512)
          while (server.isOpen)
                  pong(server.accept())
  }

  def main(args: Array[String]) {
        System.in.read()
        shutdown()
  }
}

