// pico/util.scala
// Copyright (C) 2011 Toralf Wittner

package pico.util

import java.nio.channels.{CompletionHandler => CH}
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.{LinkedBlockingQueue, ThreadPoolExecutor}

object Concurrent {

  val numOfProcessors = Runtime.getRuntime.availableProcessors

  implicit def toRunnable(a: => Unit): Runnable =
          new Runnable() {
                  def run() = a
          }

  def newThreadPool(size: Int): ThreadPoolExecutor =
          new ThreadPoolExecutor(size, size, 0L, MILLISECONDS, new LinkedBlockingQueue[Runnable])
}

object IO {

  final case class Action[A](action: CH[A, Unit] => Unit, paramType: Class[A])

  def completionHandler[A](k: A => Unit, onError: Throwable => Unit): CH[A, Unit] =
          new CH[A, Unit] {
                  def completed(value: A, attachment: Unit) {
                          k(value)
                  }
                  def failed(e: Throwable, attachment: Unit) {
                          onError(e)
                  }
          }
}

object Types {
  def safe_cast[A, B: ClassManifest](a: A): Option[B] =
          if (classManifest[B].erasure.isAssignableFrom(a.getClass))
                Some(a.asInstanceOf[B])
          else
                None

  def default[A]: A = null.asInstanceOf[A]
}

