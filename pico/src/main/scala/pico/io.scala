// pico/io.scala
// Copyright (C) 2011 Toralf Wittner

package pico

import java.nio.file.{Path, OpenOption}
import java.nio.channels.AsynchronousChannelGroup
import java.nio.channels.{AsynchronousServerSocketChannel => AsyncServerChannel}
import java.nio.channels.{AsynchronousSocketChannel => AsyncSocketChannel}
import java.nio.channels.{AsynchronousFileChannel => AsyncFileChannel}

import pico.util.Concurrent.{numOfProcessors, newThreadPool}
import pico.io.{AsyncFile, AsyncSocket, AsyncServerSocket}

import pico.util.IO._
import scala.util.continuations._
import scala.collection.JavaConverters._

final class IO (nThreads: Int = numOfProcessors) {

  private val threadpool = newThreadPool(nThreads)
  private val channelGroup = AsynchronousChannelGroup.withThreadPool(threadpool)

  def shutdown(): Unit = channelGroup.shutdownNow()

  def asyncFile(file: Path, options: OpenOption*): AsyncFile =
          new AsyncFile(AsyncFileChannel.open(file, options.toSet.asJava, threadpool))

  def asyncSocket(): AsyncSocket =
          new AsyncSocket(AsyncSocketChannel.open(channelGroup))

  def asyncServerSocket(): AsyncServerSocket =
          new AsyncServerSocket(AsyncServerChannel.open(channelGroup))

  def threadPoolQueueSize: Int = threadpool.getQueue.size
}

