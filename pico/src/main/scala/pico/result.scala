// pico/result.scala
// Copyright (C) 2011 Toralf Wittner

package pico

import scala.util.continuations._

sealed trait Result[+E, +A] {
  def fold[X](fail: E => X @cps[Step], okay: A => X @cps[Step]): X @cps[Step] =
          this match {
                  case Okay(x) => okay(x)
                  case Fail(x) => fail(x)
          }
}

final case class Okay[E, A](a: A) extends Result[E, A]
final case class Fail[E, A](e: E) extends Result[E, A]

trait Results {
  def okay[E, A](a: A): Result[E, A] = Okay(a)
  def fail[E, A](e: E): Result[E, A] = Fail(e)
}
