// pico/control.scala
// Copyright (C) 2011 Toralf Wittner

package pico

import scala.util.continuations._
import pico.util.Types._

trait Control {

  implicit def cps[A](a: => A): A @cps[Step] = a

  def tryCatch[A](a: => A @cps[Step])(e: Throwable => A @cps[Step]): A @cps[Step] =
          shift { (k: A => Step) =>
                  Try(Yield((), (c: Context) =>
                          reset {
                                  try {
                                          val x = a
                                          c.handlers = c.handlers.tail
                                          k(x)
                                  } catch {
                                          case t => {
                                                  c.handlers = c.handlers.tail
                                                  k(e(t))
                                          }
                                  }
                          })
                    , (t: Tuple2[Context, Throwable]) => reset {
                            t._1.handlers = t._1.handlers.tail
                            k(e(t._2))
                      })
          }

  def handleAll[A]: Handler[A] =
          new Handler(makeHandler((_: Throwable) => true, (t: Throwable) => throw t))

  def handle[A](exceptions: Class[_]*): Handler[A] =
          new Handler(makeHandlerFor(exceptions))

  private[pico]
  def makeHandler[E <: Throwable: ClassManifest, A](isDef: E => Boolean, f: E => A) =
          new PartialFunction[Throwable, A @cps[Step]] {
                  def isDefinedAt(x: Throwable) = safe_cast(x).exists(isDef)
                  def apply(x: Throwable): A @cps[Step] = f(safe_cast(x).get)
          }

  private[pico]
  def makeHandlerFor[A](exceptions: Seq[Class[_]]) =
          new PartialFunction[Throwable, A @cps[Step]] {
                  def isDefinedAt(x: Throwable) =
                          exceptions.exists(_.isAssignableFrom(x.getClass))

                  def apply(x: Throwable): A @cps[Step] = throw x
          }
}

final class Handler[A](h: PartialFunction[Throwable, A @cps[Step]]) {

  import Pico._

  def apply(body: => A @cps[Step]): A @cps[Step] =
          tryCatch (body) { t =>
                  if (h.isDefinedAt(t)) h(t) else cps(throw t)
          }

  def result(body: => A @cps[Step]): Result[Throwable, A] @cps[Step] = {
          val f: PartialFunction[Throwable, Result[Throwable, A] @cps[Step]] =
                  makeHandler((x: Throwable) => h.isDefinedAt(x)
                            , (x: Throwable) => fail(x))
          new Handler(f).apply(okay(body))
  }

  def log(body: => A @cps[Step]): A @cps[Step] =
          new Handler(makeHandler((x: Throwable) => h.isDefinedAt(x)
                                , (x: Throwable) => {x.printStackTrace(); default[A]}))
                  .apply(body)
}

