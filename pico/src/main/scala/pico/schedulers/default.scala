// pico/schedulers/default.scala
// Copyright (C) 2011 Toralf Wittner

package pico.schedulers

import scala.annotation.tailrec
import pico.util.Concurrent.numOfProcessors
import java.util.concurrent.LinkedBlockingQueue

import pico._
import pico.util.IO._
import pico.util.Concurrent._
import scala.util.continuations._
import scala.collection.JavaConverters._

final class DefaultScheduler(nThreads: Int = numOfProcessors) extends Scheduler {

  private val steps = new LinkedBlockingQueue[Context]
  private val threads = newThreadPool(nThreads)

  for (_ <- 0 until nThreads)
          threads.execute(runHead)

  def fork(body: => Unit @cps[Step], onShutdown: => Unit = ()): Unit = {
          val c = new Context(() => onShutdown)
          c.step = Yield((), (_: Unit) => reset { body; Done })
          steps.put(c)
  }

  def suspend(): Unit @cps[Step] = shift { (k: Unit => Step) =>
          Yield((), k)
  }

  def shutdown(): Unit = {
          threads.shutdownNow()
          for (s <- steps.asScala)
                  s.onShutdown()
  }

  def stepsQueueSize: Int = steps.size
  def threadsQueueSize: Int = threads.getQueue.size

  @tailrec
  private def runHead() {
          try handle(steps.take()) catch {
                  case _: InterruptedException => Thread.currentThread.interrupt()
                  case e                       => e.printStackTrace()
          }
          if (!Thread.currentThread.isInterrupted)
                  runHead()
  }

  private def handle(c: Context): Unit = c.step match {
          case Yield(Action(action, ptype), k) => action {
                  completionHandler((p: Any) => {
                          c.step = Yield((), (_: Unit) => k(ptype.cast(p)))
                          steps.put(c)
                  }
                , c.handlers.headOption.getOrElse(_.printStackTrace()))
          }
          case Try(Yield((), k), a) => {
                  c.handlers = ((t: Throwable) => {
                        c.step = a(c, t)
                        steps.put(c)
                  }) :: c.handlers
                  c.step = k(c)
                  steps.put(c)
          }
          case Yield(v, k) => {
                  c.step = k(v)
                  steps.put(c)
          }
          case Done => ()
  }

}

