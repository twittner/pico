// pico/package.scala
// Copyright (C) 2011 Toralf Wittner

package object pico {

  import scala.util.continuations._
  import scala.collection.generic.CanBuildFrom
  import scala.collection.{GenTraversableOnce, IterableLike}

  sealed trait Step
  final case class Yield[A, B](value: A, next: B => Step) extends Step
  final case class Try[A, B, C](yld: Yield[A, B], alt: C => Step) extends Step
  final case object Done extends Step

  // Implicit conversion to make common collection methods CPS-aware.
  // Due to the missing CPS annotations, normal foreach, map et al. are not
  // available in CPS code.
  // (taken from http://www.earldouglas.com/monadic-continuations-in-scala)
  implicit def cpsIterable[A, Repr](xs: IterableLike[A, Repr]) = new {
          def cps = new {
                  def foreach[B](f: A => Any @cps[Step]): Unit @cps[Step] = {
                          val it = xs.iterator
                          while (it.hasNext)
                                  f(it.next())
                  }

                  def map[B, C](f: A => B @cps[Step])
                               (implicit cbf: CanBuildFrom[Repr, B, C]): C @cps[Step] = {
                          val b = cbf(xs.repr)
                                  foreach(b += f(_))
                          b.result
                  }

                  def flatMap[B, C](f: A => GenTraversableOnce[B] @cps[Step])
                                   (implicit cbf: CanBuildFrom[Repr, B, C]): C @cps[Step] = {
                          val b = cbf(xs.repr)
                          for (x <- this)
                                  b ++= f(x)
                          b.result
                  }
          }
  }

}

