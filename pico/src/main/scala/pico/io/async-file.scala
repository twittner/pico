// pico/io/async-file.scala
// Copyright (C) 2011 Toralf Wittner

package pico.io

import pico.{Step, Yield}
import java.nio.ByteBuffer
import java.nio.channels.FileLock
import java.nio.channels.AsynchronousFileChannel
import java.lang.{Integer => JInt, Long => JLong}

import pico.util.IO._
import scala.util.continuations._

final class AsyncFile private[pico] (channel: AsynchronousFileChannel) {

  def force(metaData: Boolean): Unit = channel.force(metaData)

  def size(): Long = channel.size()

  def close(): Unit = channel.close()

  def isOpen(): Boolean = channel.isOpen()

  def tryLock(): Option[FileLock] = Option(channel.tryLock())

  def tryLock(position: Long, size: Long, shared: Boolean): Option[FileLock] =
          Option(channel.tryLock(position, size, shared))

  def read(dst: ByteBuffer, position: Long): JInt @cps[Step] =
          cont(Action[JInt](channel.read(dst, position, (), _), classOf[JInt]))

  def write(src: ByteBuffer, position: Long): JInt @cps[Step] =
          cont(Action[JInt](channel.write(src, position, (), _), classOf[JInt]))

  def lock(): FileLock @cps[Step] =
          cont(Action[FileLock](channel.lock((), _), classOf[FileLock]))

  def lock(position: Long, size: Long, shared: Boolean): FileLock @cps[Step] =
          cont(Action[FileLock](channel.lock(position, size, shared, (), _)
             , classOf[FileLock]))

  private def cont[A](a: Action[A]): A @cps[Step] = shift {
          (k: A => Step) => Yield(a, k)
  }

}
