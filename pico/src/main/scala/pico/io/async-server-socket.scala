// pico/io/async-server-socket.scala
// Copyright (C) 2011 Toralf Wittner

package pico.io

import pico.{Step, Yield}
import java.net.{SocketAddress, SocketOption}
import java.nio.ByteBuffer
import java.nio.channels.{AsynchronousSocketChannel => AsyncSocketChannel}
import java.nio.channels.AsynchronousServerSocketChannel
import java.util.concurrent.TimeUnit
import java.lang.{Integer => JInt, Long => JLong}

import pico.util.IO._
import scala.util.continuations._
import scala.collection.JavaConverters._

final class AsyncServerSocket private[pico] (channel: AsynchronousServerSocketChannel) {

  def bind(addr: SocketAddress): AsyncServerSocket = {
          channel.bind(addr)
          this
  }

  def bind(addr: SocketAddress, backlog: Int): AsyncServerSocket = {
          channel.bind(addr, backlog)
          this
  }

  def isOpen(): Boolean = channel.isOpen()

  def close(): Unit = channel.close()

  def getOption[A](name: SocketOption[A]): A = channel.getOption(name)

  def setOption[A](name: SocketOption[A], value: A): AsyncServerSocket = {
          channel.setOption(name, value)
          this
  }

  def supportedOptions(): Set[SocketOption[_]] =
          channel.supportedOptions().asScala.toSet

  def getLocalAddress(): SocketAddress = channel.getLocalAddress()

  def accept(): AsyncSocket @cps[Step] = new AsyncSocket(
          cont(Action[AsyncSocketChannel](channel.accept((), _)
             , classOf[AsyncSocketChannel])))

  private def cont[A](a: Action[A]): A @cps[Step] = shift {
          (k: A => Step) => Yield(a, k)
  }

}
