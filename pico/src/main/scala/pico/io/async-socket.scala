// pico/io/async-socket.scala
// Copyright (C) 2011 Toralf Wittner

package pico.io

import pico.{Step, Yield}
import java.net.{SocketAddress, SocketOption}
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousSocketChannel
import java.util.concurrent.TimeUnit
import java.lang.{Integer => JInt, Long => JLong}

import pico.util.IO._
import scala.util.continuations._
import scala.collection.JavaConverters._

final class AsyncSocket private[pico] (channel: AsynchronousSocketChannel) {

  def connect(addr: SocketAddress): Void @cps[Step] =
          cont(Action[Void](channel.connect(addr, (), _), classOf[Void]))

  def bind(local: SocketAddress): AsyncSocket = {
          channel.bind(local)
          this
  }

  def close(): Unit = channel.close()

  def isOpen(): Boolean = channel.isOpen()

  def getLocalAddress(): SocketAddress = channel.getLocalAddress()

  def getRemoteAddress(): SocketAddress = channel.getRemoteAddress()

  def getOption[A](name: SocketOption[A]): A = channel.getOption(name)

  def setOption[A](name: SocketOption[A], value: A): AsyncSocket = {
          channel.setOption(name, value)
          this
  }

  def supportedOptions(): Set[SocketOption[_]] =
          channel.supportedOptions().asScala.toSet

  def shutdownInput(): AsyncSocket = {
          channel.shutdownInput()
          this
  }

  def shutdownOutput(): AsyncSocket = {
          channel.shutdownOutput()
          this
  }

  def read(dest: ByteBuffer): JInt @cps[Step] =
          cont(Action[JInt](channel.read(dest, (), _), classOf[JInt]))

  def read(dest: ByteBuffer, timeout: Long, tunit: TimeUnit): JInt @cps[Step] =
          cont(Action[JInt](channel.read(dest, timeout, tunit, (), _), classOf[JInt]))

  def read(dests: Array[ByteBuffer]
         , offset: Int
         , length: Int
         , timeout: Long
         , tunit: TimeUnit): JLong @cps[Step] =
          cont(Action[JLong](channel.read(dests, offset, length, timeout, tunit, (), _)
             , classOf[JLong]))

  def write(src: ByteBuffer): JInt @cps[Step] =
          cont(Action[JInt](channel.write(src, (), _), classOf[JInt]))

  def write(src: ByteBuffer, timeout: Long, tunit: TimeUnit): JInt @cps[Step] =
          cont(Action[JInt](channel.write(src, timeout, tunit, (), _), classOf[JInt]))

  def write(srcs: Array[ByteBuffer]
          , offset: Int
          , length: Int
          , timeout: Long
          , tunit: TimeUnit): JLong @cps[Step] =
          cont(Action[JLong](channel.write(srcs, offset, length, timeout, tunit, (), _)
             , classOf[JLong]))

  private def cont[A](a: Action[A]): A @cps[Step] = shift {
          (k: A => Step) => Yield(a, k)
  }

}
