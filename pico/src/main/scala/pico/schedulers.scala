// pico/schedulers.scala
// Copyright (C) 2011 Toralf Wittner

package pico

import scala.util.continuations.cps

// A Scheduler executes coroutines by some scheduling
// policy (e.g. single-threaded) and provides methods for
// coroutine creation ("fork") and yielding ("suspend").
trait Scheduler {

  // Create a coroutine for the execution of the given
  // body expression. Actual evaluation happens according to
  // the concrete scheduling policy of the the scheduler class.
  def fork(body: => Unit @cps[Step], onShutdown: => Unit = ()): Unit

  // Suspend the execution of a coroutine. This should be invoked
  // from within a coroutine to yield control back to the scheduler.
  //
  // Note that coroutines are assumed to be cooperative, i.e. they
  // suspend themselves once in a while to allow execution to
  // switch to another coroutine.
  def suspend(): Unit @cps[Step]

  // Stop coroutine execution.
  def shutdown(): Unit

}

// Execution context holding shutdown handler as well as a stack of
// exception handlers plus the current execution step.
final class Context(val onShutdown: () => Unit) {
  @volatile var step: Step = Done
  @volatile var handlers: List[Throwable => Unit] = Nil
}

