// pico/pico.scala
// Copyright (C) 2011 Toralf Wittner

package pico

import scala.util.continuations._
import java.nio.file.{Path, OpenOption}
import pico.schedulers.DefaultScheduler
import pico.util.Concurrent.numOfProcessors

object Pico extends Scheduler
  with Control
  with Results
{
  private lazy val scheduler = new DefaultScheduler(
          Option(System.getProperty("pico.scheduler.threads"))
                .map(_.toInt).getOrElse(numOfProcessors))

  private lazy val io = new IO(
          Option(System.getProperty("pico.io.threads"))
                .map(_.toInt).getOrElse(numOfProcessors))

  type AsyncSocket = pico.io.AsyncSocket
  type AsyncFile = pico.io.AsyncFile
  type AsyncServerSocket = pico.io.AsyncServerSocket

  def fork(body: => Unit @cps[Step], onShutdown: => Unit = ()): Unit =
          scheduler.fork(body, onShutdown)

  def suspend(): Unit @cps[Step] =
          scheduler.suspend()

  def asyncFile(file: Path, options: OpenOption*): AsyncFile =
          io.asyncFile(file, options:_*)

  def asyncSocket(): AsyncSocket =
          io.asyncSocket()

  def asyncServerSocket(): AsyncServerSocket =
          io.asyncServerSocket()

  def shutdown(): Unit = {
          scheduler.shutdown()
          io.shutdown()
  }
}

