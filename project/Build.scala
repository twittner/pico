import sbt._
import Keys._

object BuildSettings {
        val buildOrganization = "twittner"
        val buildVersion      = "0.0.0"
        val buildScalaVersion = "2.9.1"

        val buildSettings = Defaults.defaultSettings ++ Seq(
                organization := buildOrganization
              , version      := buildVersion
              , scalaVersion := buildScalaVersion
              , shellPrompt  := ShellPrompt.buildShellPrompt
              , autoCompilerPlugins := true
              , addCompilerPlugin("org.scala-lang.plugins" % "continuations" % buildScalaVersion)
              , scalacOptions ++= Seq("-P:continuations:enable", "-deprecation", "-unchecked")
        )
}

object Dependencies {
        //val scalaz = "org.scalaz" %% "scalaz-core" % "6.0.2"
}

object WS extends Build {
        import Dependencies._
        import BuildSettings._

        lazy val pico = Project("pico", file("pico"), settings = buildSettings)
}

object ShellPrompt {
        object devnull extends ProcessLogger {
                def info (s: => String) {}
                def error (s: => String) {}
                def buffer[T] (f: => T): T = f
        }

        val current = """\*\s+([\w-]+)""".r

        def gitBranches = ("git branch --no-color" lines_! devnull mkString)

        val buildShellPrompt = (state: State) => {
                val branch = current.findFirstMatchIn(gitBranches)
                                    .map(_.group(1))
                                    .getOrElse("-")
                val proj = Project.extract(state).currentProject.id
                "%s:%s:%s> ".format(proj, branch, BuildSettings.buildVersion)
        }
}

