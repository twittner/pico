Pico - Unified Coroutines and asynchronous I/O.
===============================================

_This library provides coroutines using Scala's continuations compiler
plugin. In addition it exposes an I/O API using Java 7's asynchronous
channels._

Pico's coroutines are lightweight threads of control which can be
created in large quantities. They are cooperative, i.e. they either
explicitly yield control back to a scheduler from time to time to
allow other coroutines to be run before their own execution resumes
or they call some of the provided I/O API functions which may suspend
them implicitly. A scheduler executes all coroutines according to some
scheduling policy. The default scheduler multiplexes execution to a
fixed number of regular Java threads.

Example Usage
-------------

```scala
...
import pico.Pico._
...

val server = asyncServerSocket()

fork {
        server.bind(new InetSocketAddress(8000))
        while (server.isOpen) {
                println("waiting for connection ...")
                val client = server.accept()

                fork {
                        ...
                        val buffer = ByteBuffer.allocateDirect(1024)
                        val n = client.read(buffer)
                        ...
                }

                println("and accepting another one ...")
        }
}
```

Pico's API consists mostly of the following operations:

### fork

`fork` executes the given code block as a new coroutine. Conceptually it
is similar to creating a new thread.

### suspend

This operation yields control back to the scheduler which is responsible
for running all available coroutines. The coroutine that invokes `suspend`
stops executing and is later resumed by the scheduler again. Long-running
coroutines which do not perform I/O should invoke `suspend` regularly to
allow other coroutines to proceed in their execution.

### I/O related functionality

If you want to do I/O in your coroutines __do not use any blocking I/O
operation__! Instead Pico provides wrapper classes `AsyncFile`,
`AsyncSocket` and `AsyncServerSocket` for their respective
`java.nio.Asynchronous*Channel` classes. These classes expose almost all
methods of their underlying java.nio.Asynchronous*Channels except that
within the execution flow of a coroutine using them looks like
invoking blocking methods (no futures or CompletionHandlers need to
be written).

License
-------

This library is released under the terms of the GNU Lesser General Public License
(LGPL) Version 3.
